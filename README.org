#+OPTIONS: toc:nil
These get changed sometimes. Without these, programs I use aren't my own! They are default.

* Stuff here
- [[emacs][emacs]]
- [[vim][vim]]
- [[neovim][neovim]]
- [[alacritty][alacritty]]
- [[mpv][mpv]]

* Good emacs configurations
- https://codeberg.org/vertbyqb/dotfiles/src/master/config/emacs
- https://codeberg.org/argo/pymacs
