" This is the font.
set guifont=Consolas:h11
" This is the best gvim color scheme.
colorscheme evening

" Beautiful syntax highlighting
syntax on

" Show number
set number

" Map leader
let mapleader = ","

" No backup stuff
set noswapfile
set nobackup

" Use mouse wisely :)
set mouse=a

" Netrw
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_winsize = 30
" Toggle Vexplore with Ctrl-E
function! ToggleVExplorer()
  if exists("t:expl_buf_num")
      let expl_win_num = bufwinnr(t:expl_buf_num)
      if expl_win_num != -1
          let cur_win_nr = winnr()
          exec expl_win_num . 'wincmd w'
          close
          exec cur_win_nr . 'wincmd w'
          unlet t:expl_buf_num
      else
          unlet t:expl_buf_num
      endif
  else
      exec '1wincmd w'
      Vexplore
      let t:expl_buf_num = bufnr("%")
  endif
endfunction
map <silent> <C-E> :call ToggleVExplorer()<CR>

" Flold stuff
set viewoptions-=options
augroup remember_folds
    autocmd!
    autocmd BufWinLeave *.* if &ft !=# 'help' | mkview | endif
    autocmd BufWinEnter *.* if &ft !=# 'help' | silent! loadview | endif
augroup END

set foldmethod=manual

highlight folded guibg=darkgrey
highlight folded guifg=yellow

" Searching but better
" Ignore case when searching
set ignorecase
" When searching try to be smart about cases
set smartcase
" Highlight search results
set nohlsearch
" Makes search act like search in modern browsers
set incsearch

" set cursorline
" Gives me vertical and horizontal highlights of text.
" Vertical highlight line
set cursorline
" Horizontal highlight line
set cursorcolumn
" Colors for both lines.
highlight CursorLine ctermbg=Yellow cterm=bold guibg=#3b3b3b
highlight CursorColumn ctermbg=Yellow cterm=bold guibg=#3b3b3b

" Spell checker
map <leader>s :setlocal spell! spelllang=en_us<CR>
" Fix what the spell checker finds
noremap <silent> <leader>fix mc1z=`c

" Do space space to jump to <++>
noremap <Space><Space> /<++><Enter>"_c4l

" Replace stuff shortcut
nnoremap S :%s##g<Left><Left>

" Copy paste
set nopaste
set clipboard=unnamed

" Show text in middle
" set sidescrolloff=999
" set scrolloff=999

" NO BEEP
" Could cause problems when use the alt key.
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Wildmenu
set wildmenu
set wildmode=longest,list,full

" Auto remove spaces on save
function! <SID>StripTrailingWhitespaces()
  if !&binary && &filetype != 'diff'
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
  endif
endfun

autocmd BufWritePre,FileWritePre,FileAppendPre,FilterWritePre *
  \ :call <SID>StripTrailingWhitespaces()

" Terminal commands for nvim
if has('nvim')
  augroup vimrc_term
    autocmd!
    autocmd WinEnter term://* nohlsearch
    autocmd WinEnter term://* startinsert

    autocmd TermOpen * tnoremap <buffer> <C-w><C-h> <C-\><C-n><C-w>h
    autocmd TermOpen * tnoremap <buffer> <C-w><C-j> <C-\><C-n><C-w>j
    autocmd TermOpen * tnoremap <buffer> <C-w><C-k> <C-\><C-n><C-w>k
    autocmd TermOpen * tnoremap <buffer> <C-w><C-l> <C-\><C-n><C-w>l
    autocmd TermOpen * tnoremap <buffer> <Esc> <C-\><C-n>
    autocmd TermOpen * set nonumber
  augroup END
endif

" Tab stuffs
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set smartindent
set backspace=indent,eol,start

" Status line
" Give some space before the line starts
set laststatus=2
" Actually start the status line parameters
set statusline=
" Show the file name
set statusline+=\ FILE:
set statusline+=\ %F
" Show both the current line number and the total lines in file
set statusline+=\ LINES:
set statusline+=\ %l
set statusline+=\ %L
" Go to the right side of the status line
set statusline+=%=
" Show the progress percentage of where you are in the document
set statusline+=%p

" Open url
if (has('win32') || has('win64'))
   nmap <F4> :exec "!start <cWORD>"<cr><cr>
else
   nmap <F4> :exec "!open <cWORD>"<cr><cr>
endif

" LATEX AUTO
" Makes more latex stuff work with autocmd.
let g:tex_flavor ='tex'
" Insert a section
autocmd FileType tex inoremap ;sec \section{}<Enter><++><Enter><Esc>2kf}i
" Insert a begin
autocmd FileType tex inoremap ;beg \begin{DELRN}<Enter><++><Enter>\end{DELRN}<Esc>--www<Esc>:%s#DELRN##<Left>
" Insert a Frame
autocmd FileType tex inoremap ;fr \section{DELRN}<Enter>\begin{frame}<Enter>\frametitle{DELRN}<Enter><++><Enter>\end{frame}<Esc>

" Markdown compile to PDF
autocmd FileType markdown nmap <F9> :!C:\SGZ_Pro\Hobbys\Writing\Markdown\pandoc.exe -s -o %:r.pdf % <CR><CR>:!start %:r.pdf<CR><CR>
" Markdown compile to HTML
autocmd FileType markdown nmap <F8> :!C:\SGZ_Pro\Hobbys\Writing\Markdown\pandoc.exe -s -o %:r.html % <CR><CR>:!start %:r.html<CR><CR>

" I use vim BRO! This just gives me all my recent files.
command! Bro :enew | setl buftype=nofile |  0put =v:oldfiles
  \| nnoremap <buffer> <CR> gf | 1

" Kind of a plugin I source another file to have snake game
source ~\Appdata\Local\nvim/snake.vim
source ~\Appdata\Local\nvim/startext.vim
source ~\Appdata\Local\nvim/toc.vim
