" BIG THING THAT CHANGES YOUR START THING. Yeah don't put this. You have to
" say to other people that have never used vim that you use vim! Also that you
" support people in Uganda.
fun! Start()
    " Don't run if: we have commandline arguments, we don't have an empty
    " buffer, if we've not invoked as vim or gvim, or if we'e start in insert mode
    if argc() || line2byte('$') != -1 || v:progname !~? '^[-gmnq]\=vim\=x\=\%[\.exe]$' || &insertmode
        return
    endif

    " Start a new buffer ...
    enew

    " ... and set some options for it
    setlocal
        \ bufhidden=wipe
        \ buftype=nofile
        \ nobuflisted
        \ nocursorcolumn
        \ nocursorline
        \ nolist
        \ nonumber
        \ noswapfile
        \ norelativenumber

    " Now we can just write to the buffer, whatever you want.
    call append('$', "                               oooo$$$$$$$$$$$$oooo")
    call append('$', "                          oo$$$$$$$$$$$$$$$$$$$$$$$$o")
    call append('$', "                       oo$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o         o$   $$ o$")
    call append('$', "       o $ oo        o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o       $$ $$ $$o$")
    call append('$', "    oo $ $ $$      o$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$o       $$$o$$o$")
    call append('$', "    $$$$$$$o$     o$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$o    $$$$$$$$")
    call append('$', "      $$$$$$$    $$$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$$$$$$$$$$$$$$")
    call append('$', "      $$$$$$$$$$$$$$$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$$$$$$  $$$$$$$")
    call append('$', "       $$$$oooo$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     $$$$")
    call append('$', "        $$$   o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     $$$$o")
    call append('$', "       o$$$   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$       $$$o")
    call append('$', "       $$$    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ $$$$$$$ooooo$$$$o")
    call append('$', "      o$$$oooo$$$$$  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$   o$$$$$$$$$$$$$$$$$")
    call append('$', "      $$$$$$$$$$$$$   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     $$$$ooooooooooooo")
    call append('$', "     $$$$       $$$$    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$      o$$$")
    call append('$', "                $$$$o     $$$$$$$$$$$$$$$$$$$$$$$$$         $$$")
    call append('$', "                  $$$o          $$$$$$$$$$$$$$$           o$$$")
    call append('$', "                   $$$$o                                o$$$$")
    call append('$', "                    $$$$$$      o$$$$$$o$$$$$o        o$$$$")
    call append('$', "                      $$$$$$oo     $$$$$$o$$$$$o   o$$$$$")
    call append('$', "                         $$$$$$$oooo  $$$$o$$$$$$$$$$$$")
    call append('$', "                            $$$$$$$$$oo $$$$$$$$$$")
    call append('$', "                                    $$$$$$$$$$$$$$$")
    call append('$', "                                        $$$$$$$$$$$$")
    call append('$', "                                         $$$$$$$$$$$")
    call append('$', "                                          $$$$$$$$")
"    call append('$', "Vim is extremely bloated. But I love it.")
"    call append('$', "Life questions:")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "When will vim be my window manager?")
"    call append('$', "Why did I switch to a even more bloated version of vim called neovim?")
"    call append('$', "Will I get a job as a programmer?")
"    call append('$', "When will I will stop using windows?")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "          TEXT EDIT, have fun!")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "          TEXT EDIT, make stuff!")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "          TEXT EDIT, code stuff!")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "          TEXT EDIT, write stuff!")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "          TEXT EDIT, config stuff!")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "          Last but not least, have FUN!")
"    call append('$', "[--------------------------------------------------------]")
"    call append('$', "          Commonly used vim answers:")
"    call append('$', "          How do I quit Vim?")
"    call append('$', "          In order to quit vim RTFM")
"    call append('$', "          Type :q OR type ZZ")

    " No modifications to this buffer
    setlocal nomodifiable nomodified

    " When we go to insert mode start a new buffer, and start insert
    nnoremap <buffer><silent> e :enew<CR>
    nnoremap <buffer><silent> i :enew <bar> startinsert<CR>
    nnoremap <buffer><silent> o :enew <bar> startinsert<CR>
endfun

" Run after "doing all the startup stuff"
autocmd VimEnter * call Start()