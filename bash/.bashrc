# This will make all subdirectories in my python scripts folder in the PATH.
export PATH="$PATH:$(find "/c/SGZ_Pro/Hobbys/coding-projects/python-projects" -mindepth 1 -maxdepth 2 -type d | tr '\n' ':' | sed 's/:*$//'):/c/SGZ_Pro/Hobbys/coding-projects/fzf-scripts:/c/SGZ_Pro/Hobbys/coding-projects/bash:/c/SGZ_Pro/z-Apps_Drivers/node-v18.12.1-win-x64/node-v18.12.1-win-x64"
export PS1="[\[\033[01;34m\]\w\[\033[00m\]]\$(__git_ps1)$ "
export LC_ALL=en_US.utf8
export EDITOR=vim

man() {
    base_url="http://man.he.net/?topic="
    target_cmd="${1:?No command specified}"
    target_url="${base_url}${target_cmd}&section=all"
    curl -v --silent "${target_url}" 2>&1 | sed -n "/<pre>/,/<\/pre>/p"
}

# Got from https://github.com/gokcehan/lf/blob/master/etc/lfcd.sh
lf () {
    tmp="$(mktemp)"
    fm -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir"
            fi
        fi
    fi
}

alias dumbasslivestream="mpv https://lbry.bcow.xyz/live/content/fd60b4b0b82f7f4e870c956ab61e9137b7c54680/master.m3u8"
alias python="C:/Users/Stanl/AppData/Local/Microsoft/WindowsApps/PythonSoftwareFoundation.Python.3.10_qbz5n2kfra8p0/python.exe"
function flaskwork() {
    flask -A $1 --debug run
}
